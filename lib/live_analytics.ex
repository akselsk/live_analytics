defmodule LiveAnalytics do
  require Logger

  @moduledoc """
  Documentation for `LiveAnalytics`.
  """

  def data_from_ip(ip) do
    Logger.error("in data fromn ip #{ip}")
    token = Application.get_env(:live_analytics, :ipinfo_api_key)
    uri = "https://ipinfo.io/#{ip}?token=#{token}"

    with {:ok, obj} <- Tesla.get(uri),
    	 {:ok, body} <- Jason.decode(obj.body)
    do
        body
    	|> Map.delete("postal")
    	|> Map.delete("bogon")
    else
    err -> %{}
    end

  end

  def log_static_files_fetched_event(ip) do
    data_from_ip(ip)
    |> Map.put(:stream_id, "analytics")
    |> Map.put(:utc, DateTime.utc_now() |> DateTime.to_string())
    |> (fn map -> struct(LiveAnalytics.Events.StaticFilesFetched, map) end).()
    |> (fn event -> OtpEs.put_event("analytics", event, -1) end).()
  end

   def extract_ip(conn) do
      forwarded_for = List.first(Plug.Conn.get_req_header(conn, "x-forwarded-for"))

      if forwarded_for do
        String.split(forwarded_for, ",")
        |> Enum.map(&String.trim/1)
        |> List.first()
      else
        to_string(:inet_parse.ntoa(conn.remote_ip))
      end
    end


  def log_request(socket_id,%{"remote_ip" => ip}, req_params) do
      Logger.debug("Loggin request, req params is #{inspect(req_params)}")
      LiveAnalytics.Agent.put_ip(socket_id,ip)
  end




  def log_request(socket_id,_, %{"os_type" => _os_type} = req_params) do
      Logger.debug("Loggin request, req params is #{inspect(req_params)}")
       LiveAnalytics.Agent.put_params(socket_id, req_params)
  end

def log_user_connected(ip, device) do
    data_from_ip(ip)
    |> Map.put(:stream_id, "analytics")
    |> Map.put(:device, device)
    |> Map.put(:utc, DateTime.utc_now() |> DateTime.to_string())
    |> (fn map -> struct(LiveAnalytics.Events.UserConnected, map) end).()
    |> (fn event -> OtpEs.put_event("analytics", event, -1) end).()
  end

 def string_map_to_atom_map(inmap) do
   for {key, val} <- inmap, into: %{} do
       {String.to_existing_atom(key), val}
     end
   end


  defmodule Events do
    defmodule UserConnected do
      @derive Jason.Encoder
      defstruct [
        :stream_id,
        :utc,
        :ip,
        :city,
        :country,
        :loc,
        :region,
        :device
      ]
    end
 end

  defmodule ReadModels do
    defmodule DayAnalytics do
    use GenServer
    defmodule State do
      defstruct [
        :date,
        labels: [],
        views_2: [],
        visitors_2: [],
        views_total: 0,
        visitors_total: 0,
        visitors_by_os: %{},
        visitors_by_country: %{},
        all_ips: %{}
      ]
    end

      def start_link(_), do: GenServer.start(__MODULE__, [], name: __MODULE__)
      def get(date), do: GenServer.call(__MODULE__, {:get, date})

      def init(_) do
          OtpEs.read_and_subscribe_all_events()
          {:ok, %{}}
      end

      def handle_info({_stream_id, _event_nr, %Events.UserConnected{} = event}, all_states) do
          Logger.info("Got an event!")
          date = date_from_event(event)
         new_state =
         Map.get(all_states, date)
         |> apply_event(event)

         {:noreply, Map.put(all_states, date, new_state)}
      end

      def handle_info(_, state), do: {:noreply, state}

      def handle_call({:get, id}, _from, all_states) do
          reply = case Map.get(all_states, id) do
              nil -> %State{}
              x -> x
          end
          {:reply, reply, all_states}
      end

      def apply_event(nil, %Events.UserConnected{} = event) do
       	labels = 0..23
       	|> Enum.map(fn value ->
           	if value < 10 do
               		"0" <> Integer.to_string(value)
               	else
               		Integer.to_string(value)
               	end
               	end)

        apply_event(%State{
            date: date_from_event(event),
            labels: labels,
            views_2: List.duplicate(0,24),
            visitors_2: List.duplicate(0,24),

            },

            event)
      end

      def apply_event(%State{} = state, %Events.UserConnected{} = event) do
        timedelta = event.utc |> LiveAnalytics.datetime_string_to_hour()

        {time_index, _} = Integer.parse(timedelta)

        if LiveAnalytics.is_new_visitor?(state.all_ips, timedelta, event.ip) do
	        new_all_ip_struct = state.all_ips[timedelta]
	                            |> LiveAnalytics.nil_to_empty_map
	                            |> Map.put(event.ip, 1)
	                            |> fn timedelta_map -> Map.put(state.all_ips, timedelta, timedelta_map) end.()

        %State{ state |
          views_2: List.replace_at(state.views_2, time_index, Enum.at(state.views_2,time_index) + 1),
          visitors_2: List.replace_at(state.visitors_2, time_index, Enum.at(state.visitors_2, time_index) + 1),
          visitors_by_country: LiveAnalytics.increment_map(state.visitors_by_country,LiveAnalytics.country_from_event(event)),
          visitors_by_os: LiveAnalytics.increment_map(state.visitors_by_os, LiveAnalytics.nil_to_unknown(event.device)),
          visitors_total: state.visitors_total + 1,
          views_total: state.views_total + 1,
          all_ips: new_all_ip_struct
        }
        else
        %State{ state |
          views_2: List.replace_at(state.views_2, time_index, Enum.at(state.views_2,time_index) + 1),
          views_total: state.views_total + 1,
        }
        end


      end

      def date_from_event(event), do: LiveAnalytics.datetime_to_date_string(event.utc)


    end
  end


  def is_new_visitor?(all_ips, timedelta, ip) do
            with x when not is_nil(x) <- all_ips[timedelta],
                 y when not is_nil(y) <- x[ip]
            do
	            false
	        else
	            err -> true
	        end

  end

  def datetime_to_date_string(datetime) do
    datetime |> String.split(" ") |> Enum.at(0)
  end

  def datetime_string_to_hour(datetime) do
    datetime
    |> String.split(" ")
    |> Enum.at(1)
    |> String.split(":")
    |> Enum.at(0)
  end

  def country_from_event(event) do
    case event.country do
      nil -> "?"
      x -> x
    end
  end

  def nil_to_zero(nil), do: 0
  def nil_to_zero(x), do: x

  def nil_to_empty_map(nil), do: %{}
  def nil_to_empty_map(x), do: x

  def nil_to_unknown(nil), do: "Unknown"
  def nil_to_unknown(x), do: x

  def increment_map(map, key) do
    Map.get(map, key)
    |> nil_to_zero
    |> Kernel.+(1)
    |> (fn value -> Map.put(map, key, value) end).()
  end

  end

  defmodule LiveAnalytics.Agent do
      use GenServer

      def start_link(_opts), do: GenServer.start_link(__MODULE__, :ok, [name: __MODULE__])
      def put_ip(socket_id,ip), do: GenServer.cast(__MODULE__, {:put_ip, ip, socket_id})
      def get_nr_of_online(), do: GenServer.call(__MODULE__, :number_online)
      def get_ip_and_os(socket_id), do: GenServer.call(__MODULE__, {:get_ip_and_os, socket_id})

      def put_params(socket_id,params) do
          pid = self()
        GenServer.cast(__MODULE__, {:put_params, params, socket_id, pid})
      end

     @impl true
      def init(:ok) do
          {:ok, {%{},%{}}  }
      end

	@impl true
      def handle_call(:number_online, _from, {clients,state})  do
          nr = Map.keys(clients)
          |> length
          {:reply,nr, {clients, state}}
      end


	@impl true
      def handle_call({:get_ip_and_os, socket_id}, _from, {clients,state})  do
          {:reply, Map.get(clients, socket_id), {clients, state}}
      end

      @impl true
      def handle_cast({:put_ip, ip, socket_id}, {clients, refs}) do
          {:noreply, {Map.put(clients, socket_id, {ip, nil}), refs}}
      end

      def handle_cast({:put_params, params, socket_id, pid}, {clients, refs}) do
          ref = Process.monitor(pid)
          refs = Map.put(refs, ref, socket_id)
	        Map.get(clients, socket_id)
	        |> case do
		        nil -> {:noreply, {clients, refs}}
		        {ip, _} ->
	            LiveAnalytics.log_user_connected(ip, params["os_type"])
                {:noreply, {Map.put(clients, socket_id, {ip, params["os_type"]}), refs}}
                end
       end


        @impl true
        def handle_info({:DOWN, ref, :process, _pid, _reason}, {clients, refs}) do
          {pid, refs} = Map.pop(refs, ref)
          clients= Map.delete(clients, pid)
          {:noreply, {clients, refs}}
        end

   end



