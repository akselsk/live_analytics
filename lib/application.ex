defmodule LiveAnalytics.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      LiveAnalytics.Agent,
      LiveAnalytics.ReadModels.DayAnalytics

      # Starts a worker by calling: LiveAnalytics.Worker.start_link(arg)
      # {LiveAnalytics.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: LiveAnalytics.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
