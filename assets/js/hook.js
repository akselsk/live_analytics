var OsType = "Unknown OS";
    if (navigator.userAgent.indexOf("Win") != -1) OsType =
      "Windows OS"; 
    if (navigator.userAgent.indexOf("Mac") != -1) OsType =
      "Macintosh"; 
    if (navigator.userAgent.indexOf("Linux") != -1) OsType =
      "Linux OS"; 
    if (navigator.userAgent.indexOf("Android") != -1) OsType =
      "Android OS"; 
    if (navigator.userAgent.indexOf("like Mac") != -1) OsType =
      "iOS";


  var views_and_visitors_data= null
  var views_and_visitors_chart;
  var views_and_visitors_options = null

  var visitors_by_country_data= null
  var visitors_by_country_chart;
  var visitors_by_country_options = null;


  var visitors_by_os_data= null
  var visitors_by_os_chart;
  var visitors_by_os_options = null;


  google.charts.load('current', {'packages':['corechart']});
  // Set a callback to run when the Google Visualization API is loaded.
  google.charts.setOnLoadCallback(drawChart);
  // Callback that creates and populates a data table,
  // instantiates the pie chart, passes in the data and
  // draws it.
  function drawChart() {
    // Create the data table.
      views_and_visitors_options = {
        height: 400,
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: true
      };

      visitors_by_country_options= {
	    title: ' Visitors by Country',
        height: 200,
        legend: { position: 'top', maxLines: 3 },
      };

      visitors_by_os_options= {
	    title: ' Visitors by Operating System',
        height: 200,
        legend: { position: 'top', maxLines: 3 },
      };

        // Set chart options
        // Instantiate and draw our chart, passing in some options.
        views_and_visitors_chart= new google.visualization.ColumnChart(document.getElementById('chart_div'));
        visitors_by_country_chart= new google.visualization.PieChart(document.getElementById('country_chart_div'));
        visitors_by_os_chart= new google.visualization.PieChart(document.getElementById('os_chart_div'));

      }

// Make one function for reset width all_data, an anothr wisth the rest


let HooksChart = {
  mounted(){
    this.handleEvent("chart_data", ({views_and_visitors_raw, visitors_by_country_raw, visitors_by_os_raw}) => {
    views_and_visitors_data = new google.visualization.DataTable();
    views_and_visitors_data.addColumn('string', 'Date');
    views_and_visitors_data.addColumn('number', 'Views');
    views_and_visitors_data.addColumn('number', 'Visitors');
    views_and_visitors_data.addRows(views_and_visitors_raw)
    views_and_visitors_chart.draw(views_and_visitors_data, views_and_visitors_options)

    visitors_by_country_data= new google.visualization.arrayToDataTable(visitors_by_country_raw)
    visitors_by_os_data= new google.visualization.arrayToDataTable(visitors_by_os_raw)

    visitors_by_country_chart.draw(visitors_by_country_data, visitors_by_country_options)
    visitors_by_os_chart.draw(visitors_by_os_data, visitors_by_os_options)

    })
    }
  }
  export HooksChart;

let HooksPiechart = {
  mounted(){
    this.handleEvent("by_country", ({data, data2}) => {

    visitors_by_country_data= new google.visualization.arrayToDataTable(data)
    visitors_by_os_data= new google.visualization.arrayToDataTable(data2)

    visitors_by_country_chart.draw(visitors_by_country_data, pie_chart_options)
    visitors_by_os_chart.draw(visitors_by_os_data, pie_chart_options)
    })
    }
  }
  export HooksPiechart;


