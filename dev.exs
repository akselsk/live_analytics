# iex -S mix dev
Logger.configure(level: :debug)

# Configures the endpoint
Application.put_env(:live_analytics, DemoWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Hu4qQN3iKzTV4fJxhorPQlA/osH9fAMtbtjVS58PFgfw3ja5Z18Q/WSNR9wP4OfW",
  live_view: [signing_salt: "hMegieSe"],
  http: [port: System.get_env("PORT") || 4000],
  debug_errors: true,
  check_origin: false,
  pubsub_server: Demo.PubSub,
  watchers: [
    node: [
      "node_modules/webpack/bin/webpack.js",
      "--mode",
      System.get_env("NODE_ENV") || "production",
      "--watch-stdin",
      cd: "assets"
    ]
  ],
  live_reload: [
    patterns: [
      ~r"priv/static/.*(js|css|png|jpeg|jpg|gif|svg)$",
      ~r"lib/live_analytics_web/(live|views)/.*(ex)$",
      ~r"lib/live_analytics_web//templates/.*(ex)$"
    ]
  ]
)

defmodule DemoWeb.PageController do
  use LiveAnalyticsWeb, :controller
  import Plug.Conn

  def init(opts), do: opts

  def call(conn, :index) do
    content(conn, """
    <h2>Live Analytics Dev</h2>
    <a href="/analytics" target="_blank">Open Analytics </a>
    """)
  end

  def call(conn, :hello) do
    name = Map.get(conn.params, "name", "friend")
    content(conn, "<p>Hello, #{name}!</p>")
  end

  defp content(conn, content) do
    conn
    |> put_resp_header("content-type", "text/html")
    |> send_resp(200, "<!doctype html><html><body>#{content}</body></html>")
  end
end

defmodule DemoWeb.Router do
  use LiveAnalyticsWeb, :router
  import Phoenix.LiveView.Router

  pipeline :browser do
    plug :fetch_session
    # plug :put_csp
    # plug LiveAnalytics.AsyncLogRequest
  end

  scope "/" do
    pipe_through [:browser, :put_client_ip]

    live "/analytics", LiveAnalyticsWeb.PageLive, :index, layout: {LiveAnalyticsWeb.LayoutView, :root}
    live "/chart", LiveChartWeb.PageLive, :index
  end

  def put_client_ip(conn, _) do
    Plug.Conn.put_session(conn, :remote_ip, LiveAnalytics.extract_ip(conn))
  end

  defp put_csp(conn, _params) do
    [img_nonce, style_nonce, script_nonce] =
      for _i <- 1..3, do: 16 |> :crypto.strong_rand_bytes() |> Base.url_encode64(padding: false)

    conn
    |> assign(:img_csp_nonce, img_nonce)
    |> assign(:style_csp_nonce, style_nonce)
    |> assign(:script_csp_nonce, script_nonce)
    |> put_resp_header(
      "content-security-policy",
      "default-src; script-src 'nonce-#{script_nonce}'; style-src 'nonce-#{style_nonce}'; " <>
        "img-src 'nonce-#{img_nonce}' data: ; font-src data: ; connect-src 'self'; frame-src 'self' ;"
    )
  end
end

defmodule DemoWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :live_analytics

  @session_options [
    store: :cookie,
    key: "_live_view_key",
    secure: false,
    signing_salt: "2uaQfmEP"
  ]

  socket "/live", Phoenix.LiveView.Socket

  socket "/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phx.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/",
    from: :live_analytics,
    gzip: false,
    only: ~w(css fonts images js favicon.ico robots.txt)

  plug Phoenix.LiveReloader
  plug Phoenix.CodeReloader
  plug Plug.Session, @session_options

  plug Plug.RequestId
  plug DemoWeb.Router
end

Application.ensure_all_started(:os_mon)
# Application.ensure_all_started(:live_analytics)
Application.put_env(:phoenix, :serve_endpoints, true)

Task.start(fn ->
  children = [
    {Phoenix.PubSub, [name: Demo.PubSub, adapter: Phoenix.PubSub.PG2]},
    DemoWeb.Endpoint
  ]

  {:ok, _} = Supervisor.start_link(children, strategy: :one_for_one)
  Process.sleep(:infinity)
end)
