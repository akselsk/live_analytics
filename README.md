# LiveAnalytics

A very simple analytics tool inspired by umami.is. The data is persisted in a google cloud bucket
by using otp_es, and otp_cqrs



## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
